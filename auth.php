<?php 
	session_start();
	if ($_SESSION['auth']==TRUE) {
		echo "<script type='text/javascript'>window.location = 'cab.php'</script>";
	}
	else{
		
	include_once "includes/config.php";
	include_once "includes/header.php";
	include_once "includes/authNavigation.php";
?>
	<h1>АВТОРИЗАЦИЯ</h1>
	<form action="auth.php" method="post">
		<input type="text" name="email" required="" placeholder="Ваша почта"><br><br>
		<input type="password" name="password" required="" placeholder="Ваш пароль"><br><br>
		<input type="submit" name="submit" value="Войти">
	</form>
<?php
	include_once "includes/footer.php";
		
	if ($_POST['submit']!="") {

		$email = $_POST['email'];
		$password = password_hash($_POST['password'], PASSWORD_BCRYPT);

		$check_query = $pdo->prepare ("SELECT `p_id` FROM `persons` WHERE `p_email` = :email ");
		$check_query->execute(array('email' => $email));
		$result = $check_query->fetch();

		$check_query2 = $pdo->prepare ("SELECT `p_id` FROM `persons` WHERE `p_password` = :password ");
		$check_query2->execute(array('password' => $password));
		$result2 = $check_query2->fetch();

		if ($result2[0]==$result[0]) {
			$_SESSION['auth'] = TRUE;
			echo "<script type='text/javascript'>window.location = 'cab.php'</script>";
		}
		else {
			echo "Неверный email/пароль";
		}
	}
}
?>